package com.example.application_17

import android.os.Bundle
import android.view.Gravity
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // setContentView(R.layout.activity_main)

        val linLayout = LinearLayout(this)
        // specifying vertical orientation
        // specifying vertical orientation
        linLayout.orientation = LinearLayout.VERTICAL
        // creating LayoutParams
        // creating LayoutParams
        val linLayoutParam = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        // set LinearLayout as a root element of the screen
        // set LinearLayout as a root element of the screen
        setContentView(linLayout, linLayoutParam)

        val lpView = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        val txtView = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)

        val tv = TextView(this)
        tv.text = "Left Text"
        tv.layoutParams = lpView
        linLayout.addView(tv,txtView)
        val tv_two = TextView(this)
        tv_two.text = "Right text"
        linLayout.addView(tv_two,txtView)
        tv_two.gravity = Gravity.RIGHT

        val btn = Button(this)
        btn.setText("Button1")
        linLayout.addView(btn, lpView)

        val btn1 = Button(this)
        btn1.setText("Button2")
        linLayout.addView(btn1,lpView)
        btn1.gravity=Gravity.RIGHT



    }
}
